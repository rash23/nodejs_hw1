const express = require('express')
const router = express.Router()
const { createFile, getFiles, getFile, deleteFile, updateFile } = require('./filesService.js')

router.post('/', createFile)

router.get('/', getFiles)

router.get('/:filename', getFile)

// Other endpoints - put, delete.

router.delete('/:filename', deleteFile)

router.patch('/:filename', updateFile)

module.exports = {
	filesRouter: router,
}
